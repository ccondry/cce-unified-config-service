# cce-unified-config-service
A HydraExpress microservice to interface with the Cisco Contact Center Enterprise Unified Config APIs.

## Usage
Start the service with `node .`

To test, run `hydra-cli rest cce-unified-config-service:[post]/agents/41377/modify /temp/params.json`

### Example params.json
```json
{
  "value": "John",
  "setting": "first name",
  "email": "ccondry@cisco.com"
}
```
