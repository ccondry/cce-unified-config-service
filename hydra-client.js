module.exports = {
  changeEgainAgentAttribute: async function (hydra, skillTargetId, attribute, value) {
    // create hydra message
    let message = hydra.createUMFMessage({
      to: `egain-config-service:[post]/agents/${skillTargetId}/modify`,
      from: hydra.getServiceName() + ':/',
      body: {
        attribute,
        value
      }
    })
    // make the API request through hydra
    let response = await hydra.makeAPIRequest(message)
    // console.log(response)
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return(response)
    } else {
      throw(response)
    }
  }
}
