const hydraExpress = require('hydra-express')
const env = require('node-env-file')
const pkg = require('./package.json')
const util = require('util')
const UnifiedConfig = require('cce-unified-config')
const hydraClient = require('./hydra-client')

// load environment file
env('./.env')

// init CCE API connector
const cce = new UnifiedConfig({
  fqdn: process.env.cce_fqdn,
  username: process.env.cce_username,
  password: process.env.cce_password
})

// set up hydra
const hydra = hydraExpress.getHydra()

// set up hydra and redis config
const hydraConfig = {
  hydra: {
    serviceName: pkg.name,
    serviceIP: process.env.hydra_service_ip || '',
    servicePort: process.env.hydra_service_port || 0,
    serviceType: process.env.hydra_service_type || '',
    serviceDescription: pkg.description,
    redis: {
      url: process.env.redis_url,
      port: process.env.redis_port,
      db: process.env.redis_db
    }
  }
}

// define routes
function onRegisterRoutes() {
  var express = hydraExpress.getExpress()
  var api = express.Router()

  api.post('/agents/:agent/modify', async function(req, res) {
    // example body
    // req.body = {
    //   value: 'disable',
    //   setting: 'auto_answer',
    //   agent: '41377',
    //   email: 'ccondry@cisco.com'
    // }
    const email = req.body.email
    const agent = req.params.agent
    const setting = req.body.setting
    const value = req.body.value
    console.log(`request received from user ${email} to modify agent ${agent}`)
    try {
      switch (setting) {
        case 'auto_answer': {
          // change auto answer desk settings
          if (value === 'disable') {
            const data = await cce.changeDeskSetting(agent, process.env.default_no_auto_answer)
            console.log(`successfully modified agent ${agent} for user ${email}`)
            return res.status(202).send()
          } else if (value === 'enable') {
            const data = await cce.changeDeskSetting(agent, process.env.default_auto_answer)
            console.log(`successfully modified agent ${agent} for user ${email}`)
            return res.status(202).send()
          } else {
            const err = 'invalid value for auto_answer: ' + value
            console.log(err)
            return res.status(422).send({err})
          }
        }
        case 'password': {
          const data = await cce.changePassword(agent, value)
          console.log(`successfully modified agent ${agent} for user ${email}`)
          return res.status(202).send()
        }
        case 'first name': {
          const data = await cce.changeFirstName(agent, value)
          console.log(`successfully modified agent ${agent} for user ${email}`)
          const skillTargetId = data.refURL.split('/').pop()
          console.log('attempting to update agent in ECE database')
          await hydraClient.changeEgainAgentAttribute(hydra, skillTargetId, 'FIRST_NAME', value)
          return res.status(202).send()
        }
        case 'last name': {
          const data = await cce.changeLastName(agent, value)
          console.log(`successfully modified agent ${agent} for user ${email}`)
          const skillTargetId = data.refURL.split('/').pop()
          console.log('attempting to update agent in ECE database')
          await hydraClient.changeEgainAgentAttribute(hydra, skillTargetId, 'LAST_NAME', value)
          return res.status(202).send()
        }
        default: {
          const err = 'unknown setting ' + setting
          console.log(err)
          return res.status(422).send({err})
        }
      }
    } catch (error) {
      // ... error checks
      console.log(error)
      if (error.status) {
        return res.status(error.status).send(error)
      } else {
        return res.status(500).send({error})
      }
    }
  })

  hydraExpress.registerRoutes({
    '': api
  })
}

// start up
hydraExpress.init(hydraConfig, onRegisterRoutes)
.then(serviceInfo => {
  console.log('serviceInfo', serviceInfo)
  // return 0
})
.catch(err => console.log('err', err))
